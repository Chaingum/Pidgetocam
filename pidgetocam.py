# This Python file uses the following encoding: utf-8

# Import des modules
import RPi.GPIO as GPIO
import time
import os

# Initialisation de la numerotation, des E/S et définition du répertoire de travail
GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT, initial = GPIO.LOW)
os.chdir("/home/pi/Desktop")

# Incrémentation du fichier de comptage d'activation (bonus)
with open("activations.log", "r") as activationfile:
	counter = activationfile.read()

counter = int(counter) + 1
	
with open("activations.log", "w") as activationfile:
	activationfile.write(str(counter))

#-DEBUG
stat = GPIO.input(12)
print("Etat initial de la pin 12 = " + str(stat))
time.sleep(0.5)

# Passage de l'état de la pin 3 de LOW a HIGH, la pin fournit du 3.3V, le commutateur comute et le ventilateur s'allume
GPIO.output(12, GPIO.HIGH)

#-DEBUG
stat = GPIO.input(12)
print("Etat après montage = " + str(stat))

# Attend 2 secondes (en partant de 0) et repasse la pin 3 en LOW, le ventilateur s'arrête

time.sleep(2)
GPIO.output(12, GPIO.LOW)

#-DEBUG
stat = GPIO.input(12)
print("Etat de fin = " + str(stat))

time.sleep(0.5)
print("Pidgetocam à été activé " + str(counter) + " fois.")

GPIO.cleanup()